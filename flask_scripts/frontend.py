from flask import Flask, request, abort
import requests

app = Flask(__name__)

host = "0.0.0.0"
port = "7001"


@app.route('/new', methods=['POST'])
def new():
    if request.method == 'POST':
        post_data = request.json
        for x in post_data:
            d = post_data[x]
            req = requests.post("http://" + host + ":7001/append", json=d)
            # req = requests.get("http://consumer:7001/append", json=d)
            out = str(req.json())
        return out, 200
    else:
        abort(400)


@app.route('/view', methods=['GET'])
def view():
    if request.method == 'GET':
        # req = requests.get("http://localhost:8080/reveal")
        req = requests.get("http://127.0.0.1:7070/reveal")
        out = str(req.json())
        return out, 200
    else:
        abort(400)


@app.route('/test')
def test():
    return "IT WORKS"


if __name__ == '__main__':
    app.run(host=host, port=port)

